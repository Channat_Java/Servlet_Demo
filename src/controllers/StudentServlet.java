package controllers;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by temchannat on 5/9/18.
 */

@WebServlet("/student")
public class StudentServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        resp.setContentType("text/html");
//        PrintWriter out =  resp.getWriter();

        /*
        out.print("<h2>I am in GetMethod</h2>");
        out.print("<form method='post' action='/student'>");
        out.print("<input type='submit'/>");
        out.print("</form>");*/
        RequestDispatcher dispatcher = req.getRequestDispatcher("index.jsp");
        dispatcher.forward(req, resp);





    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//
//        resp.setContentType("text/html");
//        PrintWriter out =  resp.getWriter();
//
        String name = req.getParameter("name");
        String gender = req.getParameter("gender");
        int age = Integer.parseInt(req.getParameter("age"));

        out.print("<h2>" + name + ", " + gender + ", " + age + "</h2>");

        RequestDispatcher dispatcher = req.getRequestDispatcher("StudentInformation.jsp");
        dispatcher.forward(req, resp);

    }
}
